import sys, getopt
import os
import numpy as np
import h5py



def writefields1D(folder, file, time):
    f_r = h5py.File(folder + file, "r")
    dset = np.asarray(f_r[str(time)])

    x_array = np.ndarray((len(dset)/2))
    y_array = np.ndarray((len(dset)/2))
    a = 0
    for i in range(len(dset)/2):
        x_array[a] = dset[2*i]
        y_array[a] = dset[2*i + 1]
        a = a + 1
    f = h5py.File(folder + "output_" + str(time) + ".h5", "w")
    dset = f.create_dataset("x", data=x_array)
    dset = f.create_dataset("y", data=y_array)
    f.close()
    f_r.close()

def main():


    folder="./Dump_sf4/"
    Ncycle = 201

    for cc in xrange(0, Ncycle, 1):
        writefields1D(folder, "burger_per_bur_0_U_12800.pyg", cc)


if __name__ == "__main__":
    main()
