import numpy as np
import math
import os
import sys
from scipy import stats
import time
import h5py
from common_op_1D import avrg1D

Size_filter = 4
Npoints = 12800
Ncycle = 200
folder = "./Dump_sf4/"

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset[...]

def saveData(f, v, data):
    f = h5py.File(f, "w")
    dset = f.create_dataset(v, data=data)
    f.close()

def analyze_filter(folder,Npoints,Ncycle,sf):

    Nxf = Npoints/sf

    for cc in xrange(0, Ncycle, 1):
        start_time = time.time()
        saveEvol(folder, sf, cc)
        print "saveEvol: ", time.time() - start_time
        # Tensors with derivatives of the filtered quantities (separated for memory performance)
        start_time = time.time()

# THIS ROUTINE LOADS OR CALLS THE SFS TENSORS AND PERFORMS THE PEARSON ANALYSIS
def saveEvol(folder,sf, t):


  tauf3D(folder,sf, t)

##########################################################################   
def tauf3D(folder, sf, t):
##########################################################################    

    print "tauf init"

    u = readData(folder+"output_" + str(t) + ".h5","y")
    uf = avrg1D(u*u,sf)
    saveData(folder + "/uf_" + str(t) + ".hdf5", "uf", uf)

    u_avg = avrg1D(u,sf)
    u_tilde = u_avg * u_avg

    saveData(folder + "/u_tilde_" + str(t) + ".hdf5", "u_tilde", u_tilde)

    tau_u = u_tilde - uf
    saveData(folder + "/tau_u_" + str(t) + ".hdf5", "tau_u", tau_u)
   

############################################################################

if __name__ == "__main__":
    analyze_filter(folder,Npoints,Ncycle,Size_filter)
