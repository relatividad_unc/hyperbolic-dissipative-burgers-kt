#!/bin/bash

###
### Largar con el comando> sbatch submit....
###
### Las líneas #SBATCH configuran los recursos de la tarea
### (aunque parezcan estar comentadas)

### Nombre de la tarea
#SBATCH --job-name=onion_PHI

#SBATCH --partition=phi

### Procesos a largar.
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=54
###SBATCH --ntasks-per-socket=1
###SBATCH --cpu_bind=verbose,sockets
###SBATCH --cpus-per-task=1
### Usar el nodo completo (menos unos cores para las gpus)

### GPUs (0 o 1)
###SBATCH --gres=gpu:0

### Tiempo de ejecucion. Formato dias-horas:minutos. Maximo una semana.
#SBATCH --time 4-0:00


### Script que se ejecuta al arrancar el trabajo

### Cargar el entorno del usuario incluyendo la funcionalidad de modules
### No tocar
. /etc/profile

### Leer cuántos cores hay en el nodo que nos tocó
### No tocar

### Configurar OpenMP/MKL/etc con la cantidad de cores detectada.
### No debería ser necesario, por si prenden HyperThreading de nuevo.
export OMP_NUM_THREADS=4
export MKL_NUM_THREADS=4

### Cargar los módulos para la tarea
# FALTA: Agregar los módulos necesarios
### module load mpi/mvapich2/2.0-intel
#module load mpi/openmpi/1.8-intel_2015
#module load tools/silo/4.10.2-intel-ompi
#module load xeonphi/2015
### Largar el programa
# FALTA: Cambiar el nombre del programa
###srun ./onion_phi.exec
mpiexec -np $SLURM_NTASKS ./onion_phi.exec

