#define _POSIX_C_SOURCE 200112L
#include <stdlib.h>
#include <sys/time.h>
#include <fenv.h>
	

#include "first_macro_1d.h"  /* Where global parameters are defined */
#include "structs_1d.h"      /* Where structures are defined */
//#include "derivs_1d.h"       /* Where derivatives functions are defined */
//#include "gen_1d.h"
#include "globals.h"
#include "inidat.h"
#include "input.h"
#include "equation.h" 	
#include "integ.h"
#include "rkc.h"
#include "adisco_1d.h" 

#ifdef MPI
#include <mpi.h>
#endif

// 4M hugepage boundary
#define HUGEPAGE_SIZE (1 << 22)

/***********************   Global variables   ****************************/
struct globals globals;
/* MPI */
MPI_Comm mpi_global;

/***********************   Helper functions   ******************************/

#ifdef CATANEO
static inline void get_max_V(struct field_array *y, struct GRID_PAR *grid, FLOAT *max);
#endif
static inline void get_field_max(struct GRID_PAR *grid_1d_ptr, struct field_array *y, int FIELD_VALUE, int *I_MAX, FLOAT *max);
static inline void get_max_U(struct field_array *y, struct GRID_PAR *grid, FLOAT *max, FLOAT *time_max);
static inline void get_max_DU(struct field_array *y, struct GRID_PAR *grid, FLOAT *max, FLOAT *time_max);
		
static inline FLOAT total_variation(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *y);
		
static inline FLOAT integral_U(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *y);
		
static inline FLOAT norm_Energy(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *fields_ptr);

/* Allocate a set of fields at once in huge pages if possible */
void alloc_field_data(size_t nfields, size_t field_elems, FLOAT ** data) {
    FLOAT * memblock;

    size_t alloc_size = nfields * field_elems * sizeof(FLOAT);
    // Request a hugepage-aligned chunk of memory
    if (posix_memalign((void **) &memblock, HUGEPAGE_SIZE, alloc_size) != 0) {
        fprintf(stderr, "out of memory in posix_memaling\n");
        exit(1);
    }

#ifdef MADV_HUGEPAGE
    // Give the OS a hint
    madvise(memblock, alloc_size, MADV_HUGEPAGE);
#endif

    // Copy pointers to each field
    for (size_t i = 0; i < nfields; ++i) {
        data[i] = &memblock[i * field_elems];
    }
}

/* Free a set of fields allocated with alloc_field_data */
void free_field_data(FLOAT ** data) {
    free(*data);
}

void * safe_malloc(size_t size) {
    void * rv = malloc(size);
    if (rv == NULL) {
        fprintf(stderr, "out of memory in safe_malloc\n");
        exit(1);
    }
    return rv;
}



/***********************   MPI *******************************************/


/***********************   Global variables   ****************************/


/*
void norm_L2(struct GRID_PAR *grid_1d_ptr, 
		struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *fields_ptr);
		
void norm_Energy(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *fields_ptr);

*/

int main(int argc, char *argv[]) {

  /* variable declarations */

  struct GRID_PAR grd;
  struct GRID_PAR *grd_ptr = &grd;
  FLOAT dt, dx;
  struct field_array y;


  /*   Ploting names */

  struct PLOT_PAR plot; 
  struct PLOT_PAR *plot_ptr = &plot;

  /*   Initial data parameters */

  struct INI_PAR init_parameters;
  struct INI_PAR *init_par_ptr = &init_parameters;
  
  
  /*   Function parameters  */


  struct FUNCTION_PAR equation_parameters;
  struct FUNCTION_PAR *equation_par_ptr = &equation_parameters;

  /* Parameters coming from first_macro */


#ifdef FILE_INPUT
  FILE *file_data_ptr;
#endif

// For measuring time 

struct timeval start, stop, end;
double tiempo;





  /* ----------  Start MPI --------*/
#ifdef MPI
  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_dup(MPI_COMM_WORLD, &mpi_global);
  int processor, total_processors;
    MPI_Comm_rank(mpi_global, &processor);
    MPI_Comm_size(mpi_global, &total_processors);
  if (total_processors <= 1) {printf("total mpi processors must be bigger that 1"); exit(0);}
    MPI_float data_array[MPI_N_data];\
//  MPI_Request reqs[MPI_N_DATA*total_processors],  reqr[MPI_N_DATA*total_processors];
  //printf("I am at processor %d \n", processor);
#else
  int processor = 0;    int total_processors = 1;
#endif 



  /* Get data from web page or data file */
 



INPUT_FUNCTION(grd_ptr, equation_par_ptr, 
		 init_par_ptr, plot_ptr, processor);

//printf("out of input function at processor %d \n", processor);
//exit(0);

file_data_ptr = plot_ptr->input_data_file_ptr;

if(processor == 0){
PRINT_MACRO_VALUE(RKX)
PRINT_MACRO_VALUE(DERIV)
PRINT_MACRO_VALUE(FF)
PRINT_MACRO_VALUE(MPI_float) 
#ifdef IMAX
PRINT_MACRO_VALUE(FF)
PRINT_MACRO_VALUE(FS)
PRINT_MACRO_VALUE(FI)
#else
PRINT_MACRO_VALUE(FF)
#endif //IMAX
PRINT_MACRO_VALUE(ADISCO)
#ifdef DISSIPATION
PRINT_MACRO_VALUE(DISS)
#endif
}

    /* ------------------------------------ Allocate memory ------------------------------------ */

    /* Allocation #1:------->  auxiliary fields for rkc */

    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_temp.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.dv_sum.u);
    alloc_field_data(N_FIELDS, grd.n_grid_pts, globals.v_temp.u);
    
    
    /* Allocation #2: ------> The fields */
    alloc_field_data(N_FIELDS, grd.n_grid_pts, y.u);

    /* Allocation #3:-------> their derivatives (used in evaluating the function) */
    alloc_field_data(N_DERIVS, grd.n_grid_pts, globals.dfields.du);

    /* Allocation #4:--------->  other auxiliary fields */
    alloc_field_data(N_AUX, grd.n_grid_pts, globals.auxfields.u_aux);

    /* Allocation #5 :---------> plot vector memory */
//    plot_ptr->plot_field = (double *)safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(double));

//exit(0);


/******************  plotting section ***********************/

#ifndef MPI

#ifdef SDF
    plot_ptr->plot_field = safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(double));
#endif
#ifdef PYGRAPH
	plot_ptr->plot_field_pygraph = safe_malloc((plot_ptr->grid_plot_pts_1) * sizeof(float)*2);
#endif	
    

/* Array of names for plots */

 

/* Relation between fields and plot names */

	sprintf(plot_ptr->name[U],"U");
    sprintf(plot_ptr->window_name[U], "%s_U_%d"
	  ,plot_ptr->output_file_name,grd.n_grid_pts);
	plot_ptr->pointers[U] = U;
	  
#if (defined(CATANEO) || defined(CATANEO_TURB))
	sprintf(plot_ptr->name[V],"V");
 	sprintf(plot_ptr->window_name[V], "%s_V_%d"
	  ,plot_ptr->output_file_name,grd.n_grid_pts);
	plot_ptr->pointers[V] = V;	      
#endif 	      
	  
  plot_ptr->initial_x= grd.initial_x;
  plot_ptr->final_x = grd.final_x;


  plot_ptr->n_plots = N_PLOTS;

  





  plot_ptr->initial_time = grd.initial_time;       
  plot_ptr->final_time = grd.final_time;   
  plot_ptr->time_slice = 0;
  //plot_ptr->grid_plot_factor_1 = 1;



  /* Open output file (some times used only for compatibility) */
	printf("opening file\n");


			plot_ptr = ADISCO('O',  &plot, &grd, &y); 
//			plot_ptr = adisco_aschi_1d('O',  &plot, &grd, &y);
			
//			exit(0);
	
//  ADISCO_POINT('O', &plot, &grd, &y);

#endif // MPI

/******************   END plotting section ***********************/


  /* makes initial time-interval */

 
  dt = grd.time_step; // to be used in the equations.
  
#ifdef PERIODIC
  dx = (grd.final_x - grd.initial_x) / (double)(grd.n_grid_pts);
#else
  dx = (grd.final_x - grd.initial_x) / (double)(grd.n_grid_pts-1);
#endif

  /* sends input data to file / browser */
  
if(processor == 0){

fprintf(OUT_PUT,"<li> Total number of Time Steps = %f </br>\n",(double)(grd.data_steps*grd.int_steps));

fprintf(OUT_PUT,"<li> Number of Time Steps per unit time = %f </br>\n",1.0 / dt);

fprintf(OUT_PUT,"<li> Time_Step / Space_Step_x = (dt/dx) = %f </br>\n",dt / dx);
fprintf(OUT_PUT,"</ul>%c</br>",10);
fprintf(OUT_PUT,"\n");


fflush(stdout);

/* send input data to the screen */

#ifdef FILE_INPUT
printf("Total number of Time Steps = %f \n",(double)(grd.data_steps*grd.int_steps));

printf("Number of Time Steps per unit time = %f \n",1.0/dt);

printf("Time_Step / Space_Step_x = dt/dx = %f \n",dt/dx);
printf("\n");

fflush(stdout);
#endif

// start counting time -----------------------------------------------------

gettimeofday(&start, NULL);

}

/************************* MPI parameter fixing **************************/


#ifdef MPI


//equation_parameters.s = equation_parameters.s + 0.025; // This changes the dissipation parameter in equation


init_parameters.a1 = init_parameters.a1 + (FLOAT)processor / ((FLOAT)total_processors -1) *(init_parameters.b1 - init_parameters.a1);
//init_parameters.c0 = init_parameters.c0 + (FLOAT)processor / ((FLOAT)total_processors -1) *(init_parameters.c1 - init_parameters.c0);


#endif


/************************ end parameter fixing in MPI ********************/


// *******************************************************
// BIG LOOP START
// *******************************************************

#ifdef BIG_LOOP
//int loop, big_loop = 40;
int loop, big_loop = 20;
#else
int loop, big_loop = 1;
#endif

FLOAT V_max_init = 0.0, W_init = 0.0; // values of first derivative and the value of second derivative at maximum. 
int I_max = 0; // index of the maximum derivative value


for (loop = 1; loop <= big_loop; loop++){

FLOAT V_max = 0.0;
FLOAT U_max = 0.0;
FLOAT DU_max = 0.0;
FLOAT time_max_DU = 0.0;
FLOAT time_max_U = 0.0;
FLOAT total_var_init = 0.0;


// *******************************************************

/*     creates initial data                            */

inidat(&y,grd_ptr,&init_parameters);

/* write initial data to file                          */ 

/* plot data */


// to compute initial maximums 

DERIV(grd_ptr, y.u[U], globals.auxfields.u_aux[FU]);

get_field_max(grd_ptr, globals.auxfields.u_aux, FU, &I_max, &V_max_init);  // this is u_x maximun at t = 0.

total_var_init = total_variation(grd_ptr, equation_par_ptr, &y);   // this is the total variation of u at t = 0.

//W_init = fabs((globals.auxfields.u_aux[FU][I_max + 1] - globals.auxfields.u_aux[FU][I_max - 1])) / 2. / dx;
W_init = fabs((globals.auxfields.u_aux[FU][I_max + 1] + globals.auxfields.u_aux[FU][I_max - 1] - 2. * globals.auxfields.u_aux[FU][I_max])) / dx / dx;


#if defined(CATANEO) || defined(CATENEO_TURB) 
get_max_V(&y, grd_ptr, &V_max_init);
#endif

#ifndef MPI
#ifndef BIG_LOOP
#if defined(CATANEO) || defined(CATENEO_TURB)     

get_max_V(&y, grd_ptr, &V_max_init);
get_max_U(&y, grd_ptr, &U_max, &time_max_U);
get_max_DU(&y, grd_ptr, &DU_max, &time_max_DU);
printf("time = %f, Energy = %f, V_max = %f, U_max = %f, dU_max = %f \n", y.time, norm_Energy(grd_ptr, equation_par_ptr, &y), V_max_init, U_max, DU_max);
#endif
#endif

//    	plot_ptr = ADISCO('P',  &plot, &grd, &y); 

	
//    ADISCO_POINT('P',  &plot, &grd, &y); 
	
		plot_ptr = adisco_aschi_1d('P',  &plot, &grd, &y);

		plot_ptr = adisco_pygraph_1d('W',  &plot, &grd, &y);

#endif //MPI
  /*     creates potential function */

		// exit(0); 

/* inipot(pot_ptr,&pot_parameters, &grd, plot_ptr); */


#ifdef BUMP_TEST
exit(0);
#endif



  /* Take data_steps */ 
  


  {long int k_outer;
  for (k_outer=1; k_outer<= grd.data_steps; k_outer++) {
/*       printf("h = %f\n",h);  */
/*       printf("time = %f\n",y.a.time); */

#ifdef IMAX
integ(&y,grd_ptr,equation_par_ptr,FF,FS,FI,RKX); 
#else
integ(&y,grd_ptr,equation_par_ptr,FF,RKX); 
#endif //IMAX


//printf("time after integ in main = %f",y.time); 
//printf('time = %f, V_max = % f, dUdx_max = %f \n', time, V_max, dUdx_max);
//fflush(stdout);
/* printf("Out of integ \n");  */

#if defined(CATANEO) || defined(CATENEO_TURB)  
get_max_V(&y, grd_ptr, &V_max);
#endif

get_max_U(&y, grd_ptr, &U_max, &time_max_U);
get_max_DU(&y, grd_ptr, &DU_max, &time_max_DU);

#ifndef MPI
#ifndef BIG_LOOP

printf("time = %f, Energy = %f, V_max = %f, U_max = %f, dU_max = %f \n", y.time, norm_Energy(grd_ptr, equation_par_ptr, &y), V_max, U_max, DU_max);
#endif
#endif

      //printf("...");
      fflush(stdout);
      /* Do pointwise output */
//      ADISCO_POINT('P',  &plot, &grd, &y); 
      /* Do 1d output */
      if ((k_outer%grd.factor_1d_steps)==0){

		  plot_ptr->time_slice = k_outer;

#ifndef MPI
#ifndef BIG_LOOP
		#ifdef SDF
			plot_ptr = ADISCO('P', &plot, &grd, &y);  
		#endif
		plot_ptr = adisco_pygraph_1d('A',  &plot, &grd, &y);
		plot_ptr = ADISCO('P', &plot, &grd, &y);
#endif
#endif // MPI
		
      }

	 


/* printf("�</br>\n");  */ 
//printf("r"); 
/* printf("%c",7); */ 
fflush(stdout);
  }  }
	
//  plot_ptr = adisco_aschi_1d('P', &plot, &grd, &y); // printing the last value in aschi at .dat files.

//  plot_ptr = ADISCO('P', &plot, &grd, &y);
//		  norm_Energy(&grd, equation_par_ptr,&y);


// ************************************************
// BIG LOOP END
// ************************************************

#ifdef MPI

//printf("after loop I am at processor = %d \n", processor);
data_array[0] = 2./(FLOAT)grd.n_grid_pts;
//data_array[1] = init_parameters.c0;
data_array[1] = init_parameters.a1;
data_array[2] = equation_parameters.s;
data_array[3] = V_max_init;
data_array[4] = W_init;
data_array[5] = V_max;
data_array[6] = U_max;
data_array[7] = DU_max;
data_array[8] = time_max_DU;
data_array[9] = total_var_init;

{
int dat, proc;

if(processor != 0){
//	printf("sending data from processor %d \n", processor);
MPI_Send(&data_array, MPI_N_data, MPI_DOUBLE, 0, processor, mpi_global); 
}

if(processor == 0){
	printf("data to file in 0");
	for(dat = 0; dat < MPI_N_data; dat++){
		fprintf(OUT_PUT,"%e, ", data_array[dat]);
	}
	fprintf(OUT_PUT,"\n"); 

for (proc = 1; proc < total_processors; proc++){
MPI_Recv(&data_array, MPI_N_data, MPI_DOUBLE, proc, proc, mpi_global, MPI_STATUS_IGNORE);
//	printf("receiving data and to file from processor %d \n", proc);
	for(dat = 0; dat < MPI_N_data; dat++){
		fprintf(OUT_PUT,"%e, ", data_array[dat]);
	}
	fprintf(OUT_PUT,"\n"); 
}
}
}

#endif

#ifdef BIG_LOOP
//printf("%f, %f, %f,  %f, %f, %f, %f \n", 2./(FLOAT)grd.n_grid_pts, init_parameters.c0, equation_parameters.s, V_max_init, V_max, U_max, DU_max);
printf("%e, %e, %e,  %e, %e, %e, %e \n", 2./(FLOAT)grd.n_grid_pts, init_parameters.a1, equation_parameters.s, V_max_init, V_max, U_max, DU_max);

//equation_parameters.s = equation_parameters.s + 0.025; // This changes the dissipation parameter in equation

//init_parameters.a1 = init_parameters.a1 + 0.0010;
init_parameters.a1 = init_parameters.a1 + 0.0020;
//init_parameters.a1 = init_parameters.a1 + 0.00050;


#endif //BIG_LOOP

}
// ************************************************

//printf("after the BIG_LOOP");

if(processor == 0){
gettimeofday(&end, NULL);

tiempo = ((double) end.tv_sec + end.tv_usec / 1000000.0) - ((double) start.tv_sec + start.tv_usec / 1000000.0);


fprintf(OUT_PUT,"<ul>%c</br>",10);
fprintf(OUT_PUT,"<li> Execution time = %u secs. Time = %u", (unsigned)(clock()/CLOCKS_PER_SEC), tiempo);
fprintf(OUT_PUT,"</ul>%c</br>",10);

#ifdef FILE_INPUT
printf("\n");
printf("Execution (CPU) time = %u secs. Time = %u", (unsigned)(clock()/CLOCKS_PER_SEC), tiempo);
printf("\n");
#endif

/* close output file */
//    plot_ptr = ADISCO('C',  &plot, &grd, &y); 
//    ADISCO_POINT('C', &plot, &grd, &y);
//	plot_ptr = adisco_aschi_1d('C',  &plot, &grd, &y); 

#ifdef FILE_INPUT
  fclose(plot_ptr->input_data_file_ptr);
#endif
printf("%c",7);
printf("finishing \n");

}

#ifdef MPI
        MPI_Finalize();
#endif

return(0);
}

/*
static inline void get_max(char inst, struct field_array *y, struct GRID_PAR *grid_1d_ptr, FLOAT *max){
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
//		FLOAT xi = (*grid_1d_ptr).initial_x;
//		FLOAT xf = (*grid_1d_ptr).final_x;
//		FLOAT dt = (*grid_1d_ptr).time_step;
//		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;

	switch (inst) {// switch
		case 'V':{// case 'V' 
			for (grid_ind1 = ni_1; grid_ind1 < nf_1; ++grid_ind1){
					if (fabs(y->u[V][grid_ind1]) > (*max)){
						(*max) = (FLOAT)fabs(y->u[V][grid_ind1]);
					}
			}
		}
		break;
		
		case 'D':{
			for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){
					if (fabs(y->u[U][(grid_ind1+1) % nf_1] - y->u[U][grid_ind1] )  > (*max)){(*max) = fabs(y->u[U][(grid_ind1+1) % nf_1] - y->u[U][grid_ind1] );}
			}
		}
		break;
	}
}
* */


static inline void get_field_max(struct GRID_PAR *grid_1d_ptr, struct field_array *y, int FIELD_VALUE, int *I_MAX, FLOAT *max){
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
		int grid_ind1;
 
			for (grid_ind1 = ni_1; grid_ind1 < nf_1; ++grid_ind1){
					if (fabs(y->u[FIELD_VALUE][grid_ind1]) > (*max)){
						(*max) = (FLOAT)fabs(y->u[FIELD_VALUE][grid_ind1]);
						(*I_MAX) = grid_ind1;
					}
			}
		}
		
		
		
		
#if defined(CATANEO) || defined(CATANEO_TURB)
static inline void get_max_V(struct field_array *y, struct GRID_PAR *grid_1d_ptr, FLOAT *max){
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
//		FLOAT xi = (*grid_1d_ptr).initial_x;
//		FLOAT xf = (*grid_1d_ptr).final_x;
//		FLOAT dt = (*grid_1d_ptr).time_step;
//		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;
 
			for (grid_ind1 = ni_1; grid_ind1 < nf_1; ++grid_ind1){
					if (fabs(y->u[V][grid_ind1]) > (*max)){
						(*max) = (FLOAT)fabs(y->u[V][grid_ind1]);
					}
			}
		}
#endif

static inline void get_max_U(struct field_array *y, struct GRID_PAR *grid_1d_ptr, FLOAT *max, FLOAT*time_max){
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
//		FLOAT xi = (*grid_1d_ptr).initial_x;
//		FLOAT xf = (*grid_1d_ptr).final_x;
//		FLOAT dt = (*grid_1d_ptr).time_step;
//		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;
 
			for (grid_ind1 = ni_1; grid_ind1 < nf_1; ++grid_ind1){
					if (fabs(y->u[U][grid_ind1]) > (*max)){
						(*max) = (FLOAT)fabs(y->u[U][grid_ind1]);
						(*time_max) = y->time;
					}
			}
		}
		
		
		static inline void get_max_DU(struct field_array *y, struct GRID_PAR *grid_1d_ptr, FLOAT *max, FLOAT *time_max){
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
//		FLOAT xi = (*grid_1d_ptr).initial_x;
//		FLOAT xf = (*grid_1d_ptr).final_x;
//		FLOAT dt = (*grid_1d_ptr).time_step;
//		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		FLOAT val;
		int grid_ind1, l, stencil=7;


			for (grid_ind1 = ni_1; grid_ind1 < nf_1; ++grid_ind1){
				for (l = 1; l < stencil; ++l){
					val = fabs(y->u[U][(grid_ind1+l) % nf_1] - y->u[U][grid_ind1] );
					if ( val > (*max)){
						(*max) = val;
						(*time_max) = y->time;
						}
				}
					
			}
		}

		
		

static inline FLOAT total_variation(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *y){
			
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
//		FLOAT xi = (*grid_1d_ptr).initial_x;
//		FLOAT xf = (*grid_1d_ptr).final_x;
//		FLOAT dt = (*grid_1d_ptr).time_step;
//		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;

		FLOAT E = 0.0;;
		
			for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){
								
				E = E + fabs(y->u[U][(grid_ind1+1) % nf_1] - y->u[U][grid_ind1]);

			}
			
		return(E);
		}
		
		
static inline FLOAT integral_U(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *y){
			
						
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
		FLOAT xi = (*grid_1d_ptr).initial_x;
		FLOAT xf = (*grid_1d_ptr).final_x;
		FLOAT dt = (*grid_1d_ptr).time_step;
		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;

		FLOAT E = 0.0;;
		
			for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){
								
				E = E + y->u[U][grid_ind1];

			}
			
		return(E/h_1);
		}		

static inline FLOAT norm_Energy(struct GRID_PAR *grid_1d_ptr,
        struct FUNCTION_PAR *function_par_ptr,
		struct field_array  *y){
			
		int ni_1 = (*grid_1d_ptr).start_grid; 
		int nf_1 = (*grid_1d_ptr).final_grid; 
		FLOAT xi = (*grid_1d_ptr).initial_x;
		FLOAT xf = (*grid_1d_ptr).final_x;
		FLOAT dt = (*grid_1d_ptr).time_step;
		FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
		int grid_ind1;

		FLOAT E = 0.0;;
		
			for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){
								
#if (defined(CATANEO) || defined(CATANEO_TURB))
				E = E + function_par_ptr->s * y->u[U][grid_ind1]*y->u[U][grid_ind1];
				E = E + function_par_ptr->c * y->u[V][grid_ind1]*y->u[V][grid_ind1];
#else
				E = E + y->u[U][grid_ind1]*y->u[U][grid_ind1];
#endif
			}
			
		return(E / h_1);
		}
		
