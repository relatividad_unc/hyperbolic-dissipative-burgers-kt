
/*********************************************************************
*                                                                    *
* F -- evaluates the function f(y,t), lots of physical imput on it   *
*                                                                    *
* Parameters:                                                        *
*       fields -- pointer to field_vector from where to extract (y,t)*
*       derivs -- pointer to field_vector where to put derivatives   *
*     wave_par -- pointer to parameter struct                        *
*                                                                    *
* Returns: nothing                                                   *
*                                                                    *
*********************************************************************/
#include "first_macro_1d.h"  /* Where global parameters are defined */
#include "structs_1d.h"      /* Where structures are defined */
#include "derivs_1d.h"       /* Where derivatives functions are defined */
#include "equation.h"


/***********************************************************************/

void ff_eq(struct GRID_PAR *grid_1d_ptr,
		struct field_array *fields_ptr, 
		struct field_array *derivs_ptr,
		struct FUNCTION_PAR *function_par) {




    int ni_1 = (*grid_1d_ptr).start_grid + N_Ghost; 
    int nf_1 = (*grid_1d_ptr).final_grid - N_Ghost; 
    int n_gridpts_1 = nf_1 - ni_1;
#ifdef PERIODIC
    int n_mod = nf_1 + N_Ghost; // for the periodic case
#else
   int n_mod = nf_1 + 2*N_Ghost;
#endif

    FLOAT xi = (*grid_1d_ptr).initial_x;
    FLOAT xf = (*grid_1d_ptr).final_x;
    FLOAT dt = (*grid_1d_ptr).time_step;
    
#ifdef PERIODIC
    FLOAT one_dN1 = (xf-xi)/(FLOAT)(nf_1-ni_1);
    FLOAT h_1 = (FLOAT)(nf_1-ni_1)/(xf-xi);
#else
    FLOAT one_dN1 = (xf-xi)/(FLOAT)(nf_1-ni_1-1);
    FLOAT h_1 = (FLOAT)(nf_1-ni_1-1)/(xf-xi);
#endif
    FLOAT time;
    FLOAT x;

#ifdef F_DIFF
	FLOAT u[N_FIELDS], u_ext[N_FIELDS];
	FLOAT RP=1., RM = 0.;

#ifdef PENALTY

	FLOAT factor, L;
  	char macro_value_strg[100];

    GET_MACRO_VALUE(DERIV);
    if (strcmp(macro_value_strg,"derivS_1d")==0) {
      factor = 2.0;
    }
    else if (strcmp(macro_value_strg,"derivQ_1d")==0) {
      factor = 48.0/17.0;
    }
    else if (strcmp(macro_value_strg,"derivQ_3_1d")==0) {
      factor = 11.0/3.0;
    }
    else if (strcmp(macro_value_strg,"deriv_strand_third_order_boundaries_sixth_interior_1d")==0) {
      factor = 43200.0/13649.0;
    }
    else if (strcmp(macro_value_strg,"deriv_strand_fourth_order_boundaries_eight_interior_1d")==0) {
      factor = 5080320.0/1498139.0;
    }
    else {
      factor=2.0; printf("check factor por penalty!!!%s!!!",macro_value_strg);
    }
    
    L=(FLOAT)(nf_1-ni_1-1)/(xf-xi);
  	L=factor*L;
	
	
//	FLOAT U0[N_FIELDS], UP[N_FIELDS], UM[N_FIELDS], T0[N_FIELDS], TP[N_FIELDS], TM[N_FIELDS], V0, VP, VM;
/*   
   	{int i;
   	for (i = 0; i < N_FIELDS; ++i){
		   U0[i]=0.0;
		   UP[i]=0.0;
		   UM[i]=0.0;
		   T0[i]=0.0;
		   TP[i]=0.0;
		   TM[i]=0.0;
	   }
	}
*/

#endif // PERIODIC

#endif


//    FLOAT sx,sy,e,bx,by,B2,S2,div;
//	FLOAT u;

    
    FLOAT s = (*function_par).s; // Value for By
    FLOAT a = (*function_par).a; // Value for Bx
    FLOAT c = (*function_par).c; // Value for Sx

    FLOAT sigma = (*function_par).sigma;
    

    /* normals */

    FLOAT nx_10 = (*function_par).nx_10;
    FLOAT nx_11 = (*function_par).nx_11;

    FLOAT nx;


    
    
  /* first the time */

  time = (*fields_ptr).time; 
  (*derivs_ptr).time = (*fields_ptr).time;




  /* ghost points */

/*
  {register int i, grid_ind1;
	for (i = 0; i < N_FIELDS; ++i){
		for (grid_ind1 = 0; grid_ind1 < N_Ghost; ++grid_ind1){
			(*fields_ptr).u[i][grid_ind1] = (*fields_ptr).u[i][ni_1];
			(*fields_ptr).u[i][nf_1 + grid_ind1] = (*fields_ptr).u[i][nf_1-1];
		}
	}
  }
  */

 // This is fixed in inidat.c
 /*
	{register grid_ind1;
  		for (grid_ind1 = 0; grid_ind1 < N_Ghost; ++grid_ind1){
			(*fields_ptr).u[BY][grid_ind1] = (*fields_ptr).u[BY][ni_1];
			(*fields_ptr).u[BY][nf_1 + grid_ind1] = (*fields_ptr).u[BY][nf_1-1];
			(*fields_ptr).u[SX][grid_ind1] = 0.20;
			(*fields_ptr).u[SX][nf_1 + grid_ind1] = -0.20;
			(*fields_ptr).u[SY][grid_ind1] = (*fields_ptr).u[SY][ni_1];
			(*fields_ptr).u[SY][nf_1 + grid_ind1] = (*fields_ptr).u[SY][nf_1-1];
		}
	}
	*/

  /* inner points */
#ifdef FLUX    


#ifdef K_T

	{register int grid_ind1, i;

#pragma omp parallel for 

		for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){


    FLOAT Dp[N_FIELDS], Dm[N_FIELDS]; //Dpp[N_FIELDS], Dmm[N_FIELDS];
    

	//    FLOAT theta = 1.;   // factor for limiter
	//    FLOAT theta = 2.;   // factor for limiter
    FLOAT theta = 1.5;   // factor for limiter
 
			for (i = 0; i < N_FIELDS; ++i){ 
                
                Dp[i] = (*fields_ptr).u[i][(grid_ind1+1) % n_mod] - (*fields_ptr).u[i][grid_ind1];
//				Dpp[i] = (*fields_ptr).u[i][(grid_ind1+2) % n_mod] - (*fields_ptr).u[i][(grid_ind1+1) % n_mod];
				Dm[i] = (*fields_ptr).u[i][grid_ind1] - (*fields_ptr).u[i][(grid_ind1-1 + n_mod) % n_mod];
//				Dmm[i] = (*fields_ptr).u[i][(grid_ind1-1 + n_mod) % n_mod] - (*fields_ptr).u[i][(grid_ind1-2 + n_mod) % n_mod];
				globals.auxfields.u_aux[i][grid_ind1] = 0.5*h_1*(mysign_zero(Dp[i])+mysign_zero(Dm[i]))*MM3(Dp[i]+Dm[i],Dp[i],Dm[i],theta);
//				v_xp[i] = 0.5*h_1*(mysign_zero(Dpp[i])+mysign_zero(Dp[i]))*MM3(Dpp[i]+Dp[i],Dpp[i],Dp[i],theta);
//				v_xm[i] = 0.5*h_1*(mysign_zero(Dm[i])+mysign_zero(Dmm[i]))*MM3(Dm[i]+Dmm[i],Dm[i],Dmm[i],theta);
            }
        }
    }
                

	{register int grid_ind1, i;

#pragma omp parallel for 

	for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){


		FLOAT u_pp[N_FIELDS], u_pm[N_FIELDS], u_mp[N_FIELDS], u_mm[N_FIELDS], u[N_FIELDS], u_p[N_FIELDS], u_m[N_FIELDS];

		FLOAT v_x0[N_FIELDS], v_xp[N_FIELDS], v_xm[N_FIELDS];
		FLOAT a_p, a_m, a_pp, a_pm, a_mp, a_mm;
		FLOAT H_p[N_FIELDS], H_m[N_FIELDS];


			for (i = 0; i < N_FIELDS; ++i){ 

				v_x0[i] = globals.auxfields.u_aux[i][grid_ind1];
                v_xp[i] = globals.auxfields.u_aux[i][(grid_ind1+1) % n_mod];
                v_xm[i] = globals.auxfields.u_aux[i][(grid_ind1 + n_mod -1) % n_mod];

				u_pp[i] = (*fields_ptr).u[i][(grid_ind1+1) % n_mod] - 0.5*one_dN1*v_xp[i];
				u_pm[i] = (*fields_ptr).u[i][grid_ind1]   + 0.5*one_dN1*v_x0[i];
				u_mp[i] = (*fields_ptr).u[i][grid_ind1]   - 0.5*one_dN1*v_x0[i];
				u_mm[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -1) % n_mod] + 0.5*one_dN1*v_xm[i];
				
#ifdef SOURCE
				u[i] = (*fields_ptr).u[i][grid_ind1];
#endif

#if defined(BURGERS_DISS) || defined(BURGERS_TURB)
				u[i] = (*fields_ptr).u[i][grid_ind1];
				u_p[i] = (*fields_ptr).u[i][(grid_ind1+1) % n_mod];
				u_m[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -1) % n_mod];
#endif				
			}
			
                
				/* 
				// maximum propagation speed is 1
				*/
				//a_p = fmax((fabs(u_pp[U]) + sqrt(u_pp[U]*u_pp[U] + 4.*s*c) ), (fabs(u_pm[U]) + sqrt(u_pm[U]*u_pm[U] + 4.*s*c)) )*0.5;
				//a_m = fmax((fabs(u_mp[U]) + sqrt(u_mp[U]*u_mp[U] + 4.*s*c) ), (fabs(u_mm[U]) + sqrt(u_mm[U]*u_mm[U] + 4.*s*c)) )*0.5;
		a_p = fmax(Speed_max(u_pp, function_par),Speed_max(u_pm, function_par));
		a_m = fmax(Speed_max(u_mp, function_par),Speed_max(u_mm, function_par));
	 
			for (i = 0; i < N_FIELDS; ++i){ 
				

		
				H_p[i] = 0.5*(Fx(u_pp, grid_1d_ptr, function_par,i) + Fx(u_pm, grid_1d_ptr, function_par,i));
				H_p[i] = H_p[i] - 0.5*a_p*(u_pp[i]-u_pm[i]);
		
				H_m[i] = 0.5*(Fx(u_mp, grid_1d_ptr, function_par,i) + Fx(u_mm, grid_1d_ptr, function_par,i));
				H_m[i] = H_m[i] - 0.5*a_m*(u_mp[i]-u_mm[i]);
		
				(*derivs_ptr).u[i][grid_ind1] = -h_1 * (H_p[i] - H_m[i]);
				
				
				
#ifdef SOURCE
				(*derivs_ptr).u[i][grid_ind1] = (*derivs_ptr).u[i][grid_ind1] + Source(u,grid_1d_ptr, function_par,i,h_1);
#endif				

#if defined(BURGERS_DISS_OLD) || defined(BURGERS_TURB_OLD)
		
//				u[i] = (*fields_ptr).u[i][grid_ind1];
		
				(*derivs_ptr).u[i][grid_ind1] = (*derivs_ptr).u[i][grid_ind1] 
				+ h_1 * (Px(u, u_p, grid_1d_ptr, function_par,h_1) - Px(u_m, u, grid_1d_ptr, function_par,h_1));
		
#endif
				
			}
		}
	}

#endif // K_T

#ifdef MP5_



//printf("inside MP5_");

	{register int grid_ind1, i;

#pragma omp parallel for 

		for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){

			FLOAT u_p3[N_FIELDS], u_m3[N_FIELDS], u_p2[N_FIELDS], u_m2[N_FIELDS], u_p[N_FIELDS], u_m[N_FIELDS], u[N_FIELDS];
			FLOAT F_Pp3[N_FIELDS], F_Pm3[N_FIELDS], F_Pp2[N_FIELDS], F_Pm2[N_FIELDS], F_Pp[N_FIELDS], F_Pm[N_FIELDS], F_P[N_FIELDS];
			FLOAT F_Mp3[N_FIELDS], F_Mm3[N_FIELDS], F_Mp2[N_FIELDS], F_Mm2[N_FIELDS], F_Mp[N_FIELDS], F_Mm[N_FIELDS], F_M[N_FIELDS];
			FLOAT S_MAX;
			FLOAT F_RP[N_FIELDS], F_RM[N_FIELDS], F_LP[N_FIELDS], F_LM[N_FIELDS];
			FLOAT H_p[N_FIELDS], H_m[N_FIELDS];
    
#if defined(BURGERS_DISS) || defined(BURGERS_TURB)
			FLOAT u_p4[N_FIELDS], u_m4[N_FIELDS], u_p5[N_FIELDS], u_m5[N_FIELDS];
#endif    
        
			for (i = 0; i < N_FIELDS; ++i){ // cargamos los U[i]
				
				u[i] = (*fields_ptr).u[i][grid_ind1];
				u_p[i] = (*fields_ptr).u[i][(grid_ind1+1) % n_mod];
				u_m[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -1) % n_mod];
				u_p2[i] = (*fields_ptr).u[i][(grid_ind1+2) % n_mod];
				u_m2[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -2) % n_mod];
				u_p3[i] = (*fields_ptr).u[i][(grid_ind1+3) % n_mod];
				u_m3[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -3) % n_mod];
				
#if defined(BURGERS_DISS) || defined(BURGERS_TURB)
				u_p4[i] = (*fields_ptr).u[i][(grid_ind1+4) % n_mod];
				u_m4[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -4) % n_mod];
				u_p5[i] = (*fields_ptr).u[i][(grid_ind1+5) % n_mod];
				u_m5[i] = (*fields_ptr).u[i][(grid_ind1 + n_mod -5) % n_mod];
#endif 
			}
			
			
			// compute the maximum speed value for the stencil (if TURB we use the same stencil, no need for more)
			S_MAX = mymax_7(Speed_max(u_p3, function_par), Speed_max(u_m3, function_par), Speed_max(u_p2, function_par), 
			Speed_max(u_m2, function_par), Speed_max(u_p, function_par), Speed_max(u_m, function_par), Speed_max(u, function_par));
			
			
			
			for (i = 0; i < N_FIELDS; ++i){ // cargamos los flujos F[U[i]] y operamos


#if defined(BURGERS_DISS) || defined(BURGERS_TURB)

				// this is to add "second derivatives¨  as if they were part of a flux, the middle value is the one of the original flux.
				
				F_Pp3[i] = 0.5 * (FTurbx(u_p5, u_p4, u_p3, u_p2, u_p,  grid_1d_ptr, function_par,i,h_1) + S_MAX * u_p3[i]);
				F_Mp3[i] = 0.5 * (FTurbx(u_p5, u_p4, u_p3, u_p2, u_p,  grid_1d_ptr, function_par,i,h_1) - S_MAX * u_p3[i]);
				F_Pp2[i] = 0.5 * (FTurbx(u_p4, u_p3, u_p2, u_p,  u,    grid_1d_ptr, function_par,i,h_1) + S_MAX * u_p2[i]);
				F_Mp2[i] = 0.5 * (FTurbx(u_p4, u_p3, u_p2, u_p,  u,    grid_1d_ptr, function_par,i,h_1) - S_MAX * u_p2[i]);
				F_Pp[i]  = 0.5 * (FTurbx(u_p3, u_p2, u_p,  u,    u_m,  grid_1d_ptr, function_par,i,h_1) + S_MAX * u_p[i]);
				F_Mp[i]  = 0.5 * (FTurbx(u_p3, u_p2, u_p,  u,    u_m,  grid_1d_ptr, function_par,i,h_1) - S_MAX * u_p[i]);
				F_P[i]   = 0.5 * (FTurbx(u_p2, u_p,  u,    u_m,  u_m2, grid_1d_ptr, function_par,i,h_1) + S_MAX * u[i]);
				F_M[i]   = 0.5 * (FTurbx(u_p2, u_p,  u,    u_m,  u_m2, grid_1d_ptr, function_par,i,h_1) - S_MAX * u[i]);
				F_Pm[i]  = 0.5 * (FTurbx(u_p,  u,    u_m,  u_m2, u_m3, grid_1d_ptr, function_par,i,h_1) + S_MAX * u_m[i]);
				F_Mm[i]  = 0.5 * (FTurbx(u_p,  u,    u_m,  u_m2, u_m3, grid_1d_ptr, function_par,i,h_1) - S_MAX * u_m[i]);
				F_Pm2[i] = 0.5 * (FTurbx(u,    u_m,  u_m2, u_m3, u_m4, grid_1d_ptr, function_par,i,h_1) + S_MAX * u_m2[i]);
				F_Mm2[i] = 0.5 * (FTurbx(u,    u_m,  u_m2, u_m3, u_m4, grid_1d_ptr, function_par,i,h_1) - S_MAX * u_m2[i]);
				F_Pm3[i] = 0.5 * (FTurbx(u_m,  u_m2, u_m3, u_m4, u_m5, grid_1d_ptr, function_par,i,h_1) + S_MAX * u_m3[i]);
				F_Mm3[i] = 0.5 * (FTurbx(u_m,  u_m2, u_m3, u_m4, u_m5, grid_1d_ptr, function_par,i,h_1) - S_MAX * u_m3[i]);

#else				
				F_Pp3[i] = 0.5 * (Fx(u_p3, grid_1d_ptr, function_par,i) + S_MAX * u_p3[i]);
				F_Mp3[i] = 0.5 * (Fx(u_p3, grid_1d_ptr, function_par,i) - S_MAX * u_p3[i]);
				F_Pp2[i] = 0.5 * (Fx(u_p2, grid_1d_ptr, function_par,i) + S_MAX * u_p2[i]);
				F_Mp2[i] = 0.5 * (Fx(u_p2, grid_1d_ptr, function_par,i) - S_MAX * u_p2[i]);
				F_Pp[i]  = 0.5 * (Fx(u_p,  grid_1d_ptr, function_par,i) + S_MAX * u_p[i]);
				F_Mp[i]  = 0.5 * (Fx(u_p,  grid_1d_ptr, function_par,i) - S_MAX * u_p[i]);
				F_P[i]   = 0.5 * (Fx(u,    grid_1d_ptr, function_par,i) + S_MAX * u[i]);
				F_M[i]   = 0.5 * (Fx(u,    grid_1d_ptr, function_par,i) - S_MAX * u[i]);
				F_Pm[i]  = 0.5 * (Fx(u_m,  grid_1d_ptr, function_par,i) + S_MAX * u_m[i]);
				F_Mm[i]  = 0.5 * (Fx(u_m,  grid_1d_ptr, function_par,i) - S_MAX * u_m[i]);
				F_Pm2[i] = 0.5 * (Fx(u_m2, grid_1d_ptr, function_par,i) + S_MAX * u_m2[i]);
				F_Mm2[i] = 0.5 * (Fx(u_m2, grid_1d_ptr, function_par,i) - S_MAX * u_m2[i]);
				F_Pm3[i] = 0.5 * (Fx(u_m3, grid_1d_ptr, function_par,i) + S_MAX * u_m3[i]);
				F_Mm3[i] = 0.5 * (Fx(u_m3, grid_1d_ptr, function_par,i) - S_MAX * u_m3[i]);

#endif

				
				
				
				
				
				F_RM[i] = MP5(F_Mp2[i], F_Mp[i],  F_M[i],  F_Mm[i], F_Mm2[i]);
				F_LM[i] = MP5(F_Pm3[i], F_Pm2[i], F_Pm[i], F_P[i],  F_Pp[i]);				
				F_LP[i] = MP5(F_Pm2[i], F_Pm[i],  F_P[i],  F_Pp[i], F_Pp2[i]);				
				F_RP[i] = MP5(F_Mp3[i], F_Mp2[i], F_Mp[i], F_M[i],  F_Mm[i]);	
				
				H_p[i] = F_LP[i] + F_RP[i];
				H_m[i] = F_LM[i] + F_RM[i];			
				
				(*derivs_ptr).u[i][grid_ind1] = -h_1*(H_p[i]-H_m[i]);
				
				//printf("du = %e \n", (*derivs_ptr).u[i][grid_ind1]);
				
				
#ifdef SOURCE
				(*derivs_ptr).u[i][grid_ind1] = (*derivs_ptr).u[i][grid_ind1] + Source(u,grid_1d_ptr, function_par,i,h_1);
#endif				

#if defined(BURGERS_DISS_OLD) || defined(BURGERS_TURB_OLD)
		
		//u[i] = (*fields_ptr).u[i][grid_ind1];
		
				(*derivs_ptr).u[i][grid_ind1] = (*derivs_ptr).u[i][grid_ind1] 
				+ h_1 * (Px(u, u_p, grid_1d_ptr, function_par,h_1) - Px(u_m, u, grid_1d_ptr, function_par,h_1));
		
#endif
				
			}
		}
								
	}
				
		

#endif // MP5



   
   
   //now we set the first order KT operator at the borders
   #ifndef PERIODIC
   
   //first we average both sides
   
   (*fields_ptr).u[U][ni_1] = ((*fields_ptr).u[U][ni_1] + (*fields_ptr).u[U][nf_1-1])*0.5;
   (*fields_ptr).u[U][nf_1-1] = (*fields_ptr).u[U][ni_1];
   
   //first point
   grid_ind1 = ni_1;
   
   			a_p = 1.0; // CAMBIAR!
		
   for (i = 0; i < N_FIELDS; ++i){ 
		u_pp[i] = (*fields_ptr).u[i][(grid_ind1+1)];
		u_pm[i] = (*fields_ptr).u[i][grid_ind1];
	}
	for (i = 0; i < N_FIELDS; ++i){ 
		H_p[i] = Fx(u_pp, grid_1d_ptr, function_par,i);
		H_p[i] = H_p[i] - a_p*u_pp[i];
	
		H_m[i] = Fx(u_pm, grid_1d_ptr, function_par,i);
		H_m[i] = H_m[i] - a_p*u_pm[i];
		
		(*derivs_ptr).u[i][grid_ind1] = -h_1*(H_p[i]-H_m[i]);
	}
	
	grid_ind1 = nf_1-1;
    
			a_m = 1.0; // CAMBIAR!
			
   for (i = 0; i < N_FIELDS; ++i){ 
		u_pp[i] = (*fields_ptr).u[i][(grid_ind1)];
		u_pm[i] = (*fields_ptr).u[i][grid_ind1-1];
	}
	for (i = 0; i < N_FIELDS; ++i){ 
		H_p[i] = Fx(u_pp, grid_1d_ptr, function_par,i);
		H_p[i] = H_p[i] + a_m*u_pp[i];
	
		H_m[i] = Fx(u_pm, grid_1d_ptr, function_par,i);
		H_m[i] = H_m[i] + a_m*u_pm[i];
		
		(*derivs_ptr).u[i][grid_ind1] = -h_1*(H_p[i]-H_m[i]);
	}
	
   #endif
   
 

#endif // FLUX

#ifdef MP5_






#endif

 
#ifdef F_DIFF

// First upload the fluxes in arrays.
	{register int grid_ind1, i;

		for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){

			for (i = 0; i < N_FIELDS; ++i){ 
				u[i] = (*fields_ptr).u[i][(grid_ind1)];
			}

			for (i = 0; i < N_FLUXES; ++i){                
				globals.auxfields.u_aux[i][grid_ind1] = -Fx(u, grid_1d_ptr, function_par,i);
            }
        }

		// Take derivatives of fluxes

		for (i = 0; i < N_DERIVS; ++i){	
			DERIV(grid_1d_ptr, globals.auxfields.u_aux[i], globals.dfields.du[i]);
		}	
	}	

	{register int grid_ind1, i;
//	#pragma omp parallel for 

		for (grid_ind1 = ni_1; grid_ind1< nf_1; ++grid_ind1){

			for (i = 0; i < N_FIELDS; ++i){ 
				(*derivs_ptr).u[i][grid_ind1] = globals.dfields.du[i][grid_ind1];
				
			}
		}

   }
#endif

}

/*************************************************************************************/

 static inline FLOAT MM3(FLOAT a, FLOAT b, FLOAT c, FLOAT weight){	// (2*D0,Dp,Dm)
  weight = weight*2.;
  
  if (fabs(a) <= (weight*fabs(b))) {
  return (fabs(a) <= (weight*fabs(c))) ? fabs(a)*.5 : fabs(c);} 
  else {
  return (fabs(b) <= fabs(c)) ? fabs(b) : fabs(c);
	}
	}
	
 static inline FLOAT DMM(FLOAT a, FLOAT b){	
	return 0.5 * (mysign_zero(a) + mysign_zero(b)) * fmin(fabs(a),fabs(b));
 }
 
 static inline FLOAT DM4(FLOAT a, FLOAT b, FLOAT c, FLOAT d){	
	return 0.125 * (mysign_zero(a) + mysign_zero(b)) * fabs((mysign_zero(a) + mysign_zero(c)) * (mysign_zero(a) + mysign_zero(d)))
			* mymin_4(fabs(a),fabs(b),fabs(c),fabs(d));
 }

 static inline FLOAT MP5(FLOAT F0, FLOAT F1, FLOAT F2, FLOAT F3, FLOAT F4){
	 FLOAT b1 = 0.0166666666667;
	 FLOAT b2 = 1.3333333333333;
	 FLOAT alpha = 4.;
	 FLOAT epsm = 1.e-10;
	 
	 // remember that we are off 3 places, i-2 = 0 
	 FLOAT vor = b1 * (2. * F0 - 13. * F1 + 47. * F2 + 27. * F3 - 3. * F4);
#ifdef NO_LIMITER
	 	return vor;
#else
	 FLOAT vmp = F2 + DMM(F3 - F2, alpha * (F2 - F1));
	 if ((vor - F2) * (vor - vmp) <= epsm) {
		return vor;
	}
	 else {
		FLOAT djm1 = F0 - 2. * F1 + F2;
		FLOAT dj = F1 - 2. * F2 + F3;
		FLOAT djp1 = F2 - 2. * F3 + F4;
		FLOAT dm4jph = DM4(4. * dj - djp1, 4 * djp1 - dj, dj, djp1);
		FLOAT dm4jmh = DM4(4. * dj - djm1, 4 * djm1 - dj, dj, djm1);
		FLOAT vul = F2 + alpha * (F2 - F1);
		FLOAT vav = 0.5 * (F2 + F3);
		FLOAT vmd = vav - 0.5 * dm4jph;
		FLOAT vlc = F2 + 0.5 * (F2 - F1) + b2 * dm4jmh;
		FLOAT vmin = fmax(mymin_3(F2,F3,vmd), mymin_3(F2, vul, vlc));
		FLOAT vmax = fmin(mymax_3(F2,F3,vmd), mymax_3(F2, vul, vlc));
		return vor + DMM(vmin - vor, vmax - vor);
	}
#endif
 }

static inline FLOAT mysign_zero_m_one(FLOAT d){
    if (d > 0.) {return 1.0;}
	else return -1.0;
}

static inline FLOAT mysign_zero(FLOAT d){
    if (d > 0.) {return 1.0;}
	else if (d < 0.) return -1.0;
	else return 0.0;
}

static inline FLOAT mymin_3(FLOAT a, FLOAT b, FLOAT c){
	return fmin(fmin(a, b), c);
}

static inline FLOAT mymax_3(FLOAT a, FLOAT b, FLOAT c){
	return fmax(fmax(a, b), c);
}

static inline FLOAT mymin_4(FLOAT a, FLOAT b, FLOAT c, FLOAT d){
	return fmin(fmin(a, b), fmin(c,d));	
}

static inline FLOAT mymax_5(FLOAT a, FLOAT b, FLOAT c, FLOAT d, FLOAT e){
	return fmax(mymax_3(a, b, c), fmax(d,e));	
}

static inline FLOAT mymax_7(FLOAT a0, FLOAT a1, FLOAT a2, FLOAT a3, FLOAT a4, FLOAT a5, FLOAT a6){
	return fmax(mymax_5(a0,a1,a2,a3,a4), fmax(a5,a6));
}

/* -------------- Fluxes -------------------------*/


static inline FLOAT  Fx(FLOAT *u, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par,int i){
	FLOAT c = (*function_par).c;
	FLOAT s = (*function_par).s;
		
//	FLOAT a = (*function_par).a;

#ifdef ADVECTION
    return -c * u[U];
#endif
#ifdef BURGERS
	//printf("inside Fx");
	return -0.5*u[U]*u[U]; 
#endif
#ifdef CATANEO
		 switch(i)
    {
    case U:    return -(0.5*u[U]*u[U] + s*u[V]); break;
	case V:    return -c*u[U] ; break;
	default:   return printf("out of range in funtion Fx"); exit(0); break;
	}
#endif	
#ifdef CATANEO_TURB
		 switch(i)
    {
    case U:    return -(0.5*u[U]*u[U] + s*u[V]*u[V]/24.); break;
	case V:    return -c*u[U]*fabs(u[V]) ; break;
	default:   return printf("out of range in funtion Fx"); exit(0); break;
	}
#endif
}

#if defined(BURGERS_DISS_OLD) || defined(BURGERS_TURB_OLD)	

static inline FLOAT  Px(FLOAT *u, FLOAT *up, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par, FLOAT dx){
	
//	NOTE: THE UP IS ALWAYS THE POINT TO THE RIGHT
//	FLOAT c = (*function_par).c;
	FLOAT s = (*function_par).s;
		
//	FLOAT a = (*function_par).a;
#ifdef BURGERS_DISS_OLD
return s*(up[U] - u[U])/dx; 
#endif
#ifdef BURGERS_TURB_OLD
return s*(up[U] - u[U]) * (up[U] - u[U])/24.;
#endif
}

#endif 

#ifdef SOURCE

static inline FLOAT  Source(FLOAT *u, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par,int i, FLOAT dx){

//  Here we define sources for the equations
	FLOAT c = (*function_par).c;
//	FLOAT s = (*function_par).s;
		
//	FLOAT a = (*function_par).a;

		 switch(i)
    {
#ifdef BURGERS_DEC
    case U:    return -c * u[U]; break;
#else
    case U:    return 0.0; break;
#endif
#ifdef CATANEO
	case V:    return -c * u[V] ; break;
#endif
#ifdef CATANEO_TURB
	case V:    return -c * u[V] * fabs(u[V]) / dx ; break;
	
#endif
	default:   return printf("out of range in funtion Fx"); exit(0); break;
	}
}
#endif

static inline FLOAT Speed_max(FLOAT *u, struct FUNCTION_PAR *function_par){

//  Here we compute the maximal propagation speed of the equation, for the cases of real eigenvalues is the spectral radious of the 
//  Jacobian (when the roots have imaginary values I guess it is the maximal real part of the eigenvalues).

	FLOAT c = (*function_par).c;
	FLOAT s = (*function_par).s;		
//	FLOAT a = (*function_par).a;

	#ifdef ADVECTION
	return c;
	#endif
	
	#ifdef CATANEO
	return (fabs(u[U]) + sqrt(u[U]*u[U] + 4.*s*c))*0.5;
	#endif
	
	#ifdef CATANEO_TURB 
	return (fabs(u[U]) + sqrt(u[U] * u[U] + 8.* s * c * u[V]*u[V])) * 0.5 ; // this is not the best... but recall that V approx U_x * dx so it is small if the data is initialized.
	//return fabs(u[U]);
	#endif
	
	#ifdef BURGERS
	return fabs(u[U]); // we don't know what to put for BURGERS_TURB
	#endif
	
}

static inline FLOAT FTurbx(FLOAT *u_p2, FLOAT *u_p, FLOAT *u, FLOAT *u_m, FLOAT *u_m2, struct GRID_PAR *grid_1d_ptr, struct FUNCTION_PAR *function_par,int i, FLOAT dx){
	
/*
 * This is the function that makes fluxes which depend on more than one point (that is that depend on derivatives of the fields) *
 */
 	FLOAT s = (*function_par).s;
 	
#ifdef BURGERS_TURB

// this is dx^2 * (u'^2 - 1/4 * (u'' * dx)^2 + ....)
	FLOAT Q = (u_p[U] - u[U]) * (u[U] - u_m[U]);   
// this is dx^2 * (u'^2 + 2/3 u''' * dx^2 + ....)
//	FLOAT Q = (u_p[U] - u_m[U]) * (u_p[U] - u_m[U]) / 4.;    
// this is dx^2 * (u'^2 - 1/144 * (u'''' * dx^3)^4 + ....)
//	FLOAT Q = (- u_p2[U] + 6. * u_p[U] - 3. * u[U] - 2. * u_m[U]) * (u_m2[U] - 6. * u_m[U] + 3. * u[U] + 2. * u_p[U]) / 36.; 
//  this is centered 4th order
//	FLOAT Q = (- u_p2[U] + 8. * u_p[U] - 8. * u_m[U] +  u_m2[U]);	Q = Q * Q / 144.;

	return Fx(u,    grid_1d_ptr, function_par,i) + s / 24. * Q;
#endif 	
#ifdef BURGERS_DISS
	FLOAT Q = s*(u_p[U] - u[U])/dx;
	return  Fx(u,    grid_1d_ptr, function_par,i) + s * Q;
#endif
	
}

