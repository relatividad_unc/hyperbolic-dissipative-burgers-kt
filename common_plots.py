import os
import sys
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,AutoMinorLocator)
import matplotlib.image as mpimg
import matplotlib.ticker as mtick
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import LinearLocator, LogLocator
from scipy import ndimage


def pdf(field,Nbins):

    xbins = np.zeros(Nbins)
    maxx=np.max([-np.min(field),np.max(field)])
    counts,binh=np.histogram(field, bins=Nbins, range=([-maxx,maxx]), normed=False, weights=None, density=None)
    pdf_pdfn = counts/float(np.sum(counts))
    pdf_x = binh[0:len(binh)-1]

    return pdf_x,pdf_pdfn

          
def plot_evo(folderout,time,Etotevo,Eintevo,Ekinevo,Enstevo,Emagevo,labels,li,col,imag):

    nr = len(Ekinevo[0,:])

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        ltot=""
        lint=""
        lkin=""
        lmag=""
        if r == 0:
           ltot='Etot'
           lint='Eint'
           lkin='Ekin'
           lmag='Emag'
        plt.semilogy( time[:,r], Etotevo[:,r], linestyle="-", label=ltot, linewidth=lw)
        plt.semilogy( time[:,r], Ekinevo[:,r], linestyle=":", label=lkin, linewidth=lw)
        plt.semilogy( time[:,r], Eintevo[:,r], linestyle="--", label=lint, linewidth=lw)
        if imag==1:
            plt.semilogy( time[:,r], Emagevo[:,r], linestyle="-.", label=lmag, linewidth=lw)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Energy evolution', fontsize=fs)
    name = folderout+'evolution_energy.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        ltot=""
        lint=""
        lkin=""
        lmag=""
        if r == 0:
           ltot='Etot'
           lint='Eint'
           lkin='Ekin'
           lmag='Emag'
        plt.semilogy( time[:,r], Etotevo[:,r]/Etotevo[0,r],  label =ltot, linestyle="-", color=col[r], linewidth=lw ) 
        plt.semilogy( time[:,r], Eintevo[:,r]/Etotevo[0,r],  label =lint, linestyle="--", color=col[r], linewidth=lw ) 
        plt.semilogy( time[:,r], Ekinevo[:,r]/Etotevo[0,r],  label =lkin, linestyle=":", color=col[r], linewidth=lw ) 
        if imag==1:
            plt.semilogy( time[:,r], Emagevo[:,r]/Etotevo[0,r], linestyle="-.", label=lmag, color=col[r], linewidth=lw)
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Relative Energy', fontsize=fs)
    name = folderout+'evolution_relative.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        plt.semilogy( time[:,r], Enstevo[:,r]/Etotevo[0,r],  label =labels[r]+': Enst', linestyle=li[r], color=col[r], linewidth=lw ) 
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Enstrophy', fontsize=fs)
    name = folderout+'evolution_enstrophy.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_helicity(folderout,time,Hkinevo,Hcrossevo,labels,li,col,imag):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    nr = len(Hkinevo[0,:])

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        lkin=""
        lmag=""
        if r == 0:
           lkin='$|\int H_{kin}|$'
           lmag='$|\int H_{cross}|$'
        plt.semilogy( time[:,r], np.abs(Hkinevo[:,r]),  label =lkin, linestyle=":", color=col[r], linewidth=lw) 
        if imag==1:
            plt.semilogy( time[:,r], np.abs(Hcrossevo[:,r]), linestyle="--", label=lmag, color=col[r], linewidth=lw)
    plt.legend(loc = 4, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Helicities', fontsize=fs)
    name = folderout+'evolution_helicity.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_spectra(folderout,time,kp,spekinevo,spemagevo,labels,li,col,imag):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    nr = len(spekinevo[0,:])
    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in range(0, nr, 1):
        plt.loglog( kp, spekinevo[:,r], label = labels[r], color=col[r],linestyle="-", linewidth=lw ) 
        if imag==1:
            plt.loglog( kp, spemagevo[:,r], color=col[r],linestyle="--", linewidth=lw)

    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('k', fontsize=fs)
    plt.ylabel('Spectra', fontsize=fs)

    name = folderout+'spectra_t'+str("%i" % (10*time)).zfill(3)+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_spectra_poltor(folderout,time,kp,spekinevo,spemagevo,spekinevo_pol,spekinevo_tor,spemagevo_pol,spemagevo_tor,labels,li,col,imag):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.loglog( kp, spekinevo, label = 'Kinetic', color=col[0],linestyle="-", linewidth=lw ) 
    if imag==1:
        plt.loglog( kp, spemagevo, label = 'Magnetic', color=col[0],linestyle="--", linewidth=lw)
        plt.loglog( kp, spemagevo_pol, color=col[1],linestyle="--", linewidth=lw)
        plt.loglog( kp, spemagevo_tor, color=col[2],linestyle="--", linewidth=lw)
    plt.loglog( kp, spekinevo_pol, label = 'Poloidal', color=col[1],linestyle="-", linewidth=lw )
    plt.loglog( kp, spekinevo_tor, label = 'Toroidal', color=col[2],linestyle="-", linewidth=lw )

    plt.legend(loc = 1, fontsize=lgs)
    plt.xlabel('k', fontsize=fs)
    plt.ylabel('Spectra', fontsize=fs)

    name = folderout+'spectra_poltor_t'+str("%i" % (10*time)).zfill(3)+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print("Figure created: ", name)



def plot_bestfit_pearson(folderout,time,bestfit_pearson,labels):

    Nmodels = bestfit_pearson.shape[0]

    outname = folderout+"_bestfit_coef.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, best fit coefficient \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % bestfit_pearson[im,cc,0])
      f.write("\n")
    print "Written bestfit ceofficient evolution at: ",outname

    outname = folderout+"_bestfit_coef_err.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, best fit coefficient \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % bestfit_pearson[im,cc,1])
      f.write("\n")
    print "Written bestfit ceofficient evolution at: ",outname

    outname = folderout+"_bestfit_pearson.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, best fit Pearson \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % bestfit_pearson[im,cc,2])
      f.write("\n")

    f.close()
    print "Written bestfit Pearson evolution at: ",outname

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    ms = 6.
    mysize = (6, 6)
    mydpi = 200
    li = ["-",":","--","-."]
    styles = ['o','^','s','<','x','h','8','1','3','>']
    col = ['r', 'b', 'c', 'm', 'y']

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.locator_params(nbins=4)
    for i in xrange(0,Nmodels):
      plt.plot( time, bestfit_pearson[i,:,2],color=col[i], marker=styles[i], label=labels[i], linewidth=lw)
    plt.ylim(-1.,1.)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Pearson coefficient', fontsize=fs)
    name = folderout+"_bestfit_pearson.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name


    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.locator_params(nbins=4)
    for i in xrange(0,Nmodels):
      pos = np.ma.masked_where(bestfit_pearson[i,:,0] < 0, bestfit_pearson[i,:,0])
      neg = np.ma.masked_where(bestfit_pearson[i,:,0] > 0, abs(bestfit_pearson[i,:,0]))
      plt.plot( time, pos, color=col[i], marker=styles[i], markersize=ms, linewidth=lw, label=labels[i])
      plt.plot( time, neg, color=col[i], marker='v', markersize=ms, linewidth=lw, linestyle = '--')
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Best-fit parameters', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_bestfit_coef.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name




def plot_bestfit_pearson_onemodel(folderout,time,bestfit_pearson,labels):

    outname = folderout+"_bestfit_coef.out"
    f=open(outname,"w+")
    f.write("time, best fit coefficient \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      f.write("%12.3e" % bestfit_pearson[cc,0])
      f.write("\n")
    print "Written bestfit ceofficient evolution at: ",outname
    f.close()

    outname = folderout+"_bestfit_coef_err.out"
    f=open(outname,"w+")
    f.write("time, best fit coefficient \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      f.write("%12.3e" % bestfit_pearson[cc,1])
      f.write("\n")
    print "Written bestfit ceofficient evolution at: ",outname
    f.close()

    outname = folderout+"_bestfit_pearson.out"
    f=open(outname,"w+")
    f.write("time, best fit Pearson \n")

    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      f.write("%12.3e" % bestfit_pearson[cc,2])
      f.write("\n")

    f.close()
    print "Written bestfit Pearson evolution at: ",outname

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    ms = 6.
    mysize = (6, 6)
    mydpi = 200
    li = ["-",":","--","-."]
    styles = ['o','^','s','v','<','x','h','8','1','3','>']

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.plot( time, bestfit_pearson[:,2], linewidth=lw)
    plt.ylim(-1.,1.)
#    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Pearson coefficient', fontsize=fs)
    name = folderout+"_bestfit_pearson.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    pos = np.ma.masked_where(bestfit_pearson[:,0] < 0, bestfit_pearson[:,0])
    neg = np.ma.masked_where(bestfit_pearson[:,0] > 0, abs(bestfit_pearson[:,0]))
    plt.plot( time, pos, marker='o', markersize=ms, color='r', lw=0)
    plt.plot( time, neg, marker='o', markersize=ms, color='b', lw=0)
 #   plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Best-fit parameters', fontsize=fs)
    name = folderout+"_bestfit_coef.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name




def plot_stats(folderout,time,stats,labels):

    print  stats.shape,len(labels)
    Nmodels = stats.shape[0]

    outname = folderout+"_mean.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, mean \n")
    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % stats[im,cc,0])
      f.write("\n")
    f.close()
    print "Written mean evolution at: ",outname

    outname = folderout+"_variance.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, variance \n")
    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % stats[im,cc,1])
      f.write("\n")
    f.close()
    print "Written variance evolution at: ",outname

    outname = folderout+"_skew.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, skew \n")
    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % stats[im,cc,2])
      f.write("\n")
    f.close()
    print "Written skew evolution at: ",outname

    outname = folderout+"_kurtosis.out"
    f=open(outname,"w+")
    for im in xrange(0,Nmodels):
        f.write("%s " % labels[im])
    f.write(" : time, kurtosis \n")
    for cc in xrange(0,len(time),1):
      f.write("%f" % time[cc])
      for im in xrange(0,Nmodels):
        f.write("%12.3e" % stats[im,cc,3])
      f.write("\n")
    f.close()
    print "Written kurtosis evolution at: ",outname

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    ms = 10.
    mysize = (6, 6)
    mydpi = 200
    li = ["-",":","--","-."]
    styles = ['o','^','s','<','x','h','8','1','3','>']
    col = ['k', 'r', 'b', 'c', 'm', 'y']

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      pos = np.ma.masked_where(stats[i,:,0] < 0, stats[i,:,0])
      neg = np.ma.masked_where(stats[i,:,0] > 0, abs(stats[i,:,0]))
      plt.plot( time, pos, color=col[i], marker=styles[i], markersize=ms, linewidth=lw, label=labels[i])
      plt.plot( time, neg, color=col[i], marker='v', markersize=ms, linewidth=lw, linestyle = '--')
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Mean', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_mean.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      plt.plot( time, stats[i,:,1], color=col[i], marker=styles[i], markersize=ms, label=labels[i], linewidth=lw)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Variance', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_variance.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      pos = np.ma.masked_where(stats[i,:,2] < 0, stats[i,:,2])
      neg = np.ma.masked_where(stats[i,:,2] > 0, abs(stats[i,:,2]))
      plt.plot( time, pos, color=col[i], marker=styles[i], markersize=ms, label=labels[i], linewidth=lw)
      plt.plot( time, neg, color=col[i], marker='v', markersize=ms, linewidth=lw, linestyle = '--')
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Skew', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_skew.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for i in xrange(0,Nmodels):
      plt.plot( time, stats[i,:,3], color=col[i], marker=styles[i], markersize=ms, label=labels[i], linewidth=lw)
    plt.legend(loc = 3, fontsize=lgs)
    plt.xlabel('Time', fontsize=fs)
    plt.ylabel('Kurtosis', fontsize=fs)
    ax.set_yscale("log")
    name = folderout+"_kurtosis.png"
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name


def draw(field, fileimg, minf, maxf):
 
    field_r=ndimage.rotate(field, 90)
    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.imshow(field_r, clim=(minf,maxf))
    plt.colorbar(fraction=0.046, pad=0.04)
    plt.xticks([])
    plt.yticks([])
    plt.tight_layout()
    plt.savefig(fileimg, bbox_inches='tight')
    plt.close()
    print "Figure created: ", fileimg

def draw_labels(x,y,field, fileout, minf, maxf, xlab, ylab, labelcol):
 
#    field_r=ndimage.rotate(field, 90)
    field_r=field
    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200


    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    plt.contourf(x,y,field_r,100)#, clim=(minf,maxf))
    ax.tick_params(labelsize=lz)
    plt.locator_params(axis='x', nbins=4, format='sci')
    plt.locator_params(axis='y', nbins=4)
    ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
    ax.xaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))

    tickscol = ([minf,0.5*(minf+maxf),maxf])
    plt.colorbar(fraction=0.046, pad=0.04, ticks=tickscol, label=labelcol)
    
#    ax.w_xaxis.set_major_locator(LinearLocator(5))
#    ax.w_yaxis.set_major_locator(LinearLocator(5))
    plt.xlabel(xlab, fontsize=fs)
    plt.ylabel(ylab, fontsize=fs)
#    plt.xticks(xticks)
#    plt.yticks(([0,len(yticks)-1]),([minf,maxf]))
#    ax.set_yticks([np.min(yticks),0.5*(np.min(yticks)+np.max(yticks)),np.max(yticks)])


    plt.tight_layout()
    plt.savefig(fileout, bbox_inches='tight')
#    plt.show()
    plt.close()
    print("Figure created: ", fileout)




def plot_transfer(folderout,time,kp,tvfx,tvvpdf,tbbx,tbbpdf,spetvvevo,spetbbevo,labels,li,col,imag,rmin):

    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200
    timename = str("%i" % (10*time)).zfill(3)

    nr = len(spetvvevo[0,:])

    ylow = 1.e-4

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in xrange(rmin, nr, 1):
        plt.semilogy( tvfx[:,r], tvvpdf[:,r], label ='Tvv '+labels[r], color=col[r],linestyle=li[r], linewidth=lw ) 
    plt.ylim([ylow,1.])
    plt.legend(loc = 1, fontsize=lgs)
    plt.xlabel('$T^\\tilde{v}_{v\'}$', fontsize=fs)
    plt.ylabel('PDF', fontsize=fs)

    name = folderout+'Tvv_pdf_'+timename+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    if imag==1:
      for r in xrange(rmin, nr, 1):
        plt.semilogy( tbbx[:,r], tbbpdf[:,r], label ='Tbb '+labels[r], color=col[r],linestyle=li[r], linewidth=lw ) 
      plt.legend(loc = 1, fontsize=lgs)
      plt.ylim([ylow,1.])
      plt.xlabel('$T^{\overline{b}}_{b\'}$', fontsize=fs)
      plt.ylabel('PDF', fontsize=fs)
      name = folderout+'Tbb_pdf_t'+timename+'.png'
      plt.tight_layout()
      plt.savefig(name)
      plt.close()
      print "Figure created: ", name

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    for r in xrange(rmin, nr, 1):
        plt.loglog( kp, spetvvevo[:,r], label ='$T^\\tilde{v}_{v\'}$ '+labels[r], color=col[r],linestyle=li[r], linewidth=lw ) 

    plt.legend(loc = 1, fontsize=lgs)
    plt.xlabel('k', fontsize=fs)
    plt.ylabel('Spectra of $T^\\tilde{v}_{v\'}$', fontsize=fs)

    name = folderout+'Tvv_spectra_t'+timename+'.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()
    print "Figure created: ", name


    if imag==1:
      ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
      ax.tick_params(labelsize=lz)
      for r in xrange(rmin, nr, 1):
            plt.loglog( kp, spetbbevo[:,r], label ='$T^{\overline{b}}_{b\'}$ '+labels[r], color=col[r],linestyle=li[r], linewidth=lw )
      plt.legend(loc = 1, fontsize=lgs)
      plt.xlabel('k', fontsize=fs)
      plt.ylabel('Spectra of $T^{\overline{b}}_{b\'}$', fontsize=fs)
      name = folderout+'Tbb_spectra_t'+timename+'.png'
      plt.tight_layout()
      plt.savefig(name)
      plt.close()
      print "Figure created: ", name



def plot_field_over_time(folderout, time, field):
    
    lw=3.
    fs = 20
    lgs = 14
    lz = 16
    mysize = (6, 6)
    mydpi = 200

    ff, ax = plt.subplots(figsize=mysize, dpi=mydpi) 
    ax.tick_params(labelsize=lz)
    plt.plot(time, field)
    plt.legend(loc = 1, fontsize=lgs)
    plt.xlabel('t', fontsize=fs)
    plt.ylabel('tau/u', fontsize=fs)
    name = folderout+'Tau_div_u_over_time.png'
    plt.tight_layout()
    plt.savefig(name)
    plt.close()

