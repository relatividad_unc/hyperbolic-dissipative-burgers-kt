import os
import sys
import numpy as np
import math
#from scipy import stats
import scipy.sparse
import h5py
from common_op_1D import GradDotGrad, pearson_scalar, avrg1D, grad1D
from common_plots import plot_bestfit_pearson_onemodel, plot_field_over_time

Size_filter = 4
Npoints = 12800
Ncycle = 200
folder = "./Dump_sf4/"
folderout = "./Analysis_sf4/"
dx = 0.0001562
dt = 1

def readData(f, v):
    f = h5py.File(f, "r")
    dset = np.asarray(f[v])
    f.close()
    return dset[...]

def saveData(f, v, data):
    f = h5py.File(f, "w")
    dset = f.create_dataset(v, data=data)
    f.close()

def fit_filter(folder,folderout,Ncycle,sf,dxf, dt):

    bestfit_pearson = np.zeros((2, Ncycle,3))
    taudivu = np.zeros((Ncycle))

    for cc in xrange(0, Ncycle, 1):
        if not os.path.exists(folderout):
            os.makedirs(folderout)
        bestfit_pearson[:,cc,:], taudivu[cc] = fit3DCalculation(folder,folderout,dxf, cc, sf)

        saveData(folderout + "/bestfit_pearson_" + str(cc) + ".hdf5", "bestfit_pearson", bestfit_pearson[:,cc,:])
        print "end of fitting"

    if (Ncycle > 1):

      plot_bestfit_pearson_onemodel(folderout+"/tau_u_grad",xrange(0, Ncycle, 1),bestfit_pearson[0,:],"")
      plot_bestfit_pearson_onemodel(folderout+"/tau_u_dis",xrange(0, Ncycle, 1),bestfit_pearson[1,:],"")
      plot_field_over_time(folderout+"/", dt * np.asarray(xrange(0, Ncycle, 1)), taudivu)

    return


# THIS ROUTINE LOADS OR CALLS THE SFS TENSORS AND PERFORMS THE PEARSON ANALYSIS
def fit3DCalculation(folder, folder_save,dxf, t, sf):

    tau_u_grad(folder,dxf, t, sf)
    tau_u_dis(folder,dxf, t, sf)

    bestfit_pearson = np.zeros((2,3))

    print "============================================"
    print " GRADIENT MODELS FITS "
    print "============================================"

    bestfit_pearson[0,:] = gradientModelFits_tau_u(folder, t)
    bestfit_pearson[1,:] = gradientModelFits_dis_tau_u(folder, t)

    tau_u = readData(folder+"/tau_u_" + str(t) + ".hdf5", "tau_u")
    u = readData(folder+"output_" + str(t) + ".h5","y")
    avg_u = avrg1D(u,sf)
    taudivu = np.mean(tau_u/avg_u)

    return bestfit_pearson, taudivu

##########################################################
def gradientModelFits_tau_u(folder, t):
    print "tau_u_grad"
    tau_u = readData(folder+"/tau_u_" + str(t) + ".hdf5", "tau_u")
    tau_u_grad = readData(folder+"/tau_u_grad_" + str(t) + ".hdf5", "tau_u_grad")
    pearson,pvalue,coeff,coeff_err = pearson_scalar(tau_u,tau_u_grad)

    bp = np.zeros((3))
    bp[0] = coeff 
    bp[1] = coeff_err 
    bp[2] = pearson 

    return bp

def gradientModelFits_dis_tau_u(folder, t):
    print "tau_u_grad"
    tau_u = readData(folder+"/tau_u_" + str(t) + ".hdf5", "tau_u")
    tau_u_dis = readData(folder+"/tau_u_dis_" + str(t) + ".hdf5", "tau_u_dis")
    pearson,pvalue,coeff,coeff_err = pearson_scalar(tau_u,tau_u_dis)

    bp = np.zeros((3))
    bp[0] = coeff 
    bp[1] = coeff_err 
    bp[2] = pearson 

    return bp

##########################################################
def tau_u_grad(folder,dxf, t, sf):

    u = readData(folder+"output_" + str(t) + ".h5","y")
    uavg = avrg1D(u,sf)

    Nxf = uavg.shape[0]

    tau_u_grad = np.zeros((Nxf))
    
    tau_u_grad[:] = -dxf*dxf/12 * GradDotGrad(uavg,uavg, dxf)

    saveData(folder + "/tau_u_grad_" + str(t) + ".hdf5", "tau_u_grad", tau_u_grad)

##########################################################
def tau_u_dis(folder,dxf, t, sf):

    u = readData(folder+"output_" + str(t) + ".h5","y")
    uavg = avrg1D(u,sf)

    Nxf = uavg.shape[0]

    tau_u_grad = np.zeros((Nxf))
    
    tau_u_grad[:] = dxf*dxf * grad1D(uavg, dxf)

    saveData(folder + "/tau_u_dis_" + str(t) + ".hdf5", "tau_u_dis", tau_u_grad)



if __name__ == "__main__":
    fit_filter(folder,folderout,Ncycle,Size_filter, dx, dt)
